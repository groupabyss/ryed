<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['record_id','name'];

    public function record(){
        return $this->belongsTo(Record::class);
    }
}
