<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;  
use App\Http\Requests\StoreRecordRequest;
use App\People;
use App\Record;
use App\Document;
use App\Authorization;

class RecordsController extends Controller
{
    public function getRecords(Request $request){
        $records = Record::whereHas('peoples', function ($query) use ($request) {
                            $query->where('ci', 'like',"%" .$request->name . "%");
                           })
                         ->orderBy('id', 'DESC')
                         ->with('peoples','documents','authorization')
                         ->paginate($request->sort);

        return [
            'pagination' => [
                'total'         => $records->total(),
                'current_page'  => $records->currentPage(),
                'per_page'      => $records->perPage(),
                'last_page'     => $records->lastPage(),
                'from'          => $records->firstItem(),
                'to'            => $records->lastItem(),
            ],
            'records' => $records
        ];
    } 
    public function store(StoreRecordRequest $request){
        //record
        $new_record = new Record();
		$new_record->correlative = $request->correlative;
        $new_record->save();
            
        $people = $this->toolPeople($request);
        $new_record->peoples()->attach($people->id);
        //documents
        foreach ($request->documents as $document){
			$new_document            = new Document();
			$new_document->name      = $document;
			$new_document->record_id = $new_record->id;
			$new_document->save();
		}
        return $new_record;
    }
    public function updated(Request $request){
         //record
         $record = Record::find($request->id);
         $record->delivery_date = date("Y-m-d");
         $record->observation   = $request->observation;
         $record->update();
         $people = $this->toolPeople($request);
         if ($people->id != $record->peoples[0]->id) {
            $record->peoples()->attach($people->id,['type' => "other"]);
            
            $authorization = new Authorization();
            $authorization->record_id = $request->id;
            $authorization->name = $request->authorization;
            $authorization->save();
         } 
         
         return $record;
    } 

    public function toolPeople($request){
        //people
        $people = People::where("ci",$request->ci)->get();
        if ($people->count() == 0) {
            $new_people = new People();
            $new_people->name     = $request->name;
            $new_people->lastname = $request->lastname;
            $new_people->ci       = $request->ci;
            $new_people->save();
            return $new_people;
        }
        return $people->first();
    }
    public function print (Request $request){
        $first = $request->first;
        $last = $request->last;
        $records = Record::whereBetween('delivery_date', [$first, $last])->get();
        $view =  \View::make('pdf.reports',compact("records","first","last"))->render();                
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Reporte-exportado-el-'.date("Y-m-d").'.pdf');        
        dd($view);
    }
}
