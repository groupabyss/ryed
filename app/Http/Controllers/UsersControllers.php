<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Auth;
use App\User;
class UsersControllers extends Controller
{
    public function index()
    {
        return view("users.index");
    }

    public function getUsers(Request $request){
        $users = User::where('ci', 'like',"%" .$request->name . "%")
         ->where("id","!=",Auth::user()->id)
         ->orderBy('id', 'DESC')
         ->paginate($request->sort);
        
        return [
        'pagination' => [
        'total'         => $users->total(),
        'current_page'  => $users->currentPage(),
        'per_page'      => $users->perPage(),
        'last_page'     => $users->lastPage(),
        'from'          => $users->firstItem(),
        'to'            => $users->lastItem(),
        ],
        'users' => $users
        ];
    } 
   
    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->ci = $request->ci;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->save();
        return ;
    }

   
    public function update(UpdateUserRequest $request)
    {
        $user =User::findOrFail($request->id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->role = $request->role;
        
        if ($user->password != null) {
            $user->password = bcrypt($request->password);
        }
        $user->update();
        return ;
    }

    
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return ;
    }
}
