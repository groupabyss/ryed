<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecordRequest extends FormRequest
{
    public function attributes()
    {
        return [
            "name"        => "nombre",
            "lastname"    => "apellido",
            "ci"          => "cedula",
            "correlative" => "correlativo",
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            "name"        => "required",
            "lastname"    => "required",
            "ci"          => "required",
            "correlative" => "required|unique:records",
        ];
    }

}
