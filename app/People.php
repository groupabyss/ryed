<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $fillable = ['name', 'last_name','ci'];
 
    public function records(){
        return $this->belongsToMany(Record::class)->withPivot('type');
    }

    public function getRouteKeyName(){
        return "ci";
    }
}
