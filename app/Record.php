<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable = ['status','delivery_date','observation','correlative'];

    public function peoples(){
       return $this->belongsToMany(People::class)->withPivot('type');
    }
    public function authorization(){
        return $this->hasOne(Authorization::class);
    }
    public function documents(){
        return $this->hasMany(Document::class);
    }
}
