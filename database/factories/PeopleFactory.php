<?php

use Faker\Generator as Faker;

$factory->define(App\People::class, function (Faker $faker) {
    return [
        "name" => $faker->name,
        "lastname" => $faker->lastname(),
        "ci"=> rand(100000,299999),
    ];
});
