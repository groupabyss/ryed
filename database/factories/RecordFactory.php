<?php

use Faker\Generator as Faker;

$factory->define(App\Record::class, function (Faker $faker) {
    return [
        'correlative'=>rand(0000,9999),
        'status'=>'received',
        'observation' => $faker->sentence(5),
        'created_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});
