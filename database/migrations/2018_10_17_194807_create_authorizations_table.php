<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorizations', function (Blueprint $table) {
            $table->increments('id');
            /* records */
            $table->unsignedInteger('record_id');
            $table->foreign('record_id')
                ->references('id')
                ->on('records')
                ->onDelete('cascade')
                ->onUpdated('cascade');
            $table->enum('name',['Partida de nacimiento','Poder notariado'])->default('Partida de nacimiento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authorizations');
    }
}
