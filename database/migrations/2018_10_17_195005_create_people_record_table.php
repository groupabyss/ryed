<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_record', function (Blueprint $table) {
            $table->increments('id');
            /* registros */
            $table->unsignedInteger('record_id');
            $table->foreign('record_id')
                ->references('id')
                ->on('records')
                ->onDelete('cascade')
                ->onUpdated('cascade');
            /* personas */
            $table->unsignedInteger('people_id');
            $table->foreign('people_id')
                ->references('id')
                ->on('people')
                ->onDelete('cascade')
                ->onUpdated('cascade');
            $table->enum('type',['owner','other'])->default('owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_record');
    }
}
