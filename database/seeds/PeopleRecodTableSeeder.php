<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker; 
class PeopleRecodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker = Faker::create(); 
        
        $peopleIds = App\People::pluck('id')->all(); 
        $recordIds = App\Record::pluck('id')->all(); 

        for($i=1; $i < 30; $i++) { 

        DB::table('people_record')->insert([ 
        'people_id' => $peopleIds[$i], 
        'record_id' => $recordIds[$i] 
        ]); 

        }
    }
}
