const phanel = new Vue({
    el: "#phanel",
    created() {
        this.loadPage();
    },
    data: {
        route: APP_URL,
        records: [],
        pagination: {
            'total': 0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0
        },
        offset: 1,
        search: "",
        sort: 10,
        //records data
        record: {
            id: 0,
            action: "receive",
            ci: "",
            people_id: 0,
            name: null,
            lastname: null,
            correlative: null,
            documents: [""],
            quantity: 1,
            observation: "",
            disable: {
                ci: false,
                names: true,
                record: false,
                observation: true,
                authorization: true,
            },
            authorization: "Partida de nacimiento"
        },
        info_record: {
            correlative: null,
            peoples: [{
                ci: null,
                name: null,
                lastanme: null
            }],
            documents: [],
            observation: null,
            autorization: null
        },
        document_list: [],
        range_f: null,
        range_l: null,
    },
    methods: {
        loadPage() {
            swal({
                title: "Cargando",
                text: "espere un momento mientras se cargan los registros",
                icon: this.route + "/img/loader.gif",
                button: {
                    text: "Ok",
                    value: false,
                    closeModal: false,
                },
                closeOnClickOutside: false,
                closeOnEsc: false,
            });
            this.getRecords();
        },
        getRecords(page) {
            let routed_records = this.route + "/records?page=" + page + "&sort=" + this.sort + "&name=" + this.search;;
            axios.get(routed_records).then(response => {
                this.records = response.data.records.data;
                this.pagination = response.data.pagination;
                swal.close();
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
            });
        },
        changePage: function (page) {
            this.pagination.current_page = page;
            this.getRecords(page);
        },
        changeAction(action, data = null) {
            this.record.action = action;
            this.clearDataModalRecord();
            if (action == "receive") {
                this.record.ci = "";
                this.record.disable.ci = false;
                this.record.disable.names = true;
                this.record.disable.record = false;
                this.record.disable.observation = true;
                this.record.disable.authorization = true;
            } else {
                swal({
                    title: "Disculpe",
                    text: "La persona que va a retirar es el dueño o un tercero con authorization.",
                    icon: "info",
                    buttons: ["Persona autorizada", "Dueño"],
                }).then((owner) => {
                    if (owner) {
                        this.record.ci = data.peoples[0].ci;
                        this.record.people_id = data.peoples[0].id;
                        this.record.name = data.peoples[0].name;
                        this.record.lastname = data.peoples[0].lastname;

                        this.record.disable.ci = true;
                        this.record.disable.names = true;
                        this.record.disable.authorization = true;

                    } else {
                        this.record.ci = "";
                        this.record.disable.ci = false;
                        this.record.disable.names = true;
                        this.record.disable.authorization = false;
                    }

                    this.record.id = data.id;
                    this.record.correlative = data.correlative;
                    this.record.documents = [];
                    data.documents.forEach(document => {
                        this.record.documents.push(document.name);
                    });
                    this.record.quantity = this.record.documents.length;
                    this.record.disable.record = true;
                    this.record.disable.observation = false;
                    $("#modal-record").modal("show");
                });
            }
        },
        setDocumentList(documents) {
            let list = ""
            documents.forEach((element, index) => {
                list = list + "documento #" + (index + 1) + " : " + element.name + "\n";
            });
            this.alert("Lista", list, "", "");
        },
        //records
        searchPeople() {
            if (this.record.ci.length == 0) {
                toastr.warning("Debe colocar una cedula");
                return;
            }
            ruta_search_people = this.route + "/peoples/" + this.record.ci;
            this.loading("Espere", "Buscando persona", this.route + "/img/loader.gif");
            axios.get(ruta_search_people).then(response => {
                this.record.people_id = response.data.id;
                this.record.ci = response.data.ci;
                this.record.name = response.data.name;
                this.record.lastname = response.data.lastname;
                this.record.disable.ci = true;
                swal.close();
            }).catch(errors => {
                if (errors.request.status == 404) {
                    swal({
                        title: "Nueva Cedula ",
                        text: "Asegurese de que el numero de cedula " + this.record.ci + " ingresado es correcto y presione aceptar para crear un registro con esta nueva cedula",
                        icon: "info",
                        buttons: ["cancelar", "Aceptar"],
                    }).then((acept) => {
                        if (acept) {
                            this.record.disable.ci = true;
                            this.record.disable.names = false;
                        }
                    });
                }
            });
        },
        charge_documents() {
            if (this.record.quantity > 0) {
                if (this.record.documents.length < this.record.quantity) {
                    let add_documents = (this.record.quantity - this.record.documents.length)
                    for (var i = 0; i < add_documents; i++) {
                        this.record.documents.push("");
                    }
                } else if (this.record.documents.length > this.record.quantity) {
                    let remove_documents = (this.record.documents.length - this.record.quantity)
                    for (var i = 0; i < remove_documents; i++) {
                        this.record.documents.pop();
                    }
                }
            } else {
                toastr.info("Debe Colocar al menos un documento");
                this.record.quantity = 1;
            }
        },
        saveRecord() {
            this.loading("Procesando", "espere mientras se validan y almacenan los datos", this.route + "/img/loader.gif");
            route_store_record = this.route + "/records";
            axios.post(route_store_record, this.record).then(response => {
                this.alert("Exito !", "Registro con el correlativo " + response.data.correlative + " Creado con exito", "success");
                $("#modal-record").modal("hide");
                this.getRecords();
            }).catch(errors => {
                if (errors.response.status == 422) {
                    Object.values(errors.response.data.errors).forEach(element => {
                        toastr.error(element);
                        swal.close();
                    });;
                } else {
                    console.log(errors.response);
                }
            });

        },
        //delivery
        saveDelivery() {
            this.loading("Procesando", "espere mientras se validan y almacenan los datos", this.route + "/img/loader.gif");
            route_store_delivery = this.route + "/records";
            axios.put(route_store_delivery, this.record).then(response => {
                this.alert("Exito !", "Registro de la entrega de los documentos con el correlativo " + response.data.correlative + " Creado con exito", "success");
                $("#modal-record").modal("hide");
                this.getRecords();
            }).catch(errors => {
                console.log(errors.response);
            });

        },
        show_info(record) {
            this.info_record.correlative = record.correlative;
            this.info_record.documents = record.documents;
            this.info_record.observation = record.observation;

            this.info_record.peoples[0] = {
                name: record.peoples[0].name,
                lastname: record.peoples[0].lastname,
                ci: record.peoples[0].ci
            };
            if (record.peoples.length > 1) {
                this.info_record.peoples.push({
                    name: record.peoples[1].name,
                    lastname: record.peoples[1].lastname,
                    ci: record.peoples[1].ci
                });
                this.info_record.authorization = record.authorization.name;
            }
            $("#modal-details").modal("show");
        },
        validate_date() {
            if (this.range_f == "" || this.range_f == null || this.range_l == "" || this.range_l == null) {
                return
            } else {
                var first = Date.parse(this.range_f); //01 de Octubre del 2013
                var last = Date.parse(this.range_l); //03 de Octubre del 2013

                if (first > last) {
                    toastr.warning("Desde no puede ser mayor que hasta");
                    this.range_f = this.range_l;
                }
            }
        },
        print() {
            if (this.range_f == "" || this.range_f == null || this.range_l == "" || this.range_l == null) {
                toastr.warning("Seleccione un rango de fechas");
                return;
            }
            toastr.info("Imprimiendo el reporte , tome en cuenta que mientras mas largo sea el rango, mas tarda en imprimir")
            $("#form-print").submit();
            $("#modal-reports").modal("hide");
        },
        //recycle
        loading(p_title, p_text, p_icon) {
            swal({
                title: p_title,
                text: p_text,
                icon: p_icon,
                button: {
                    text: "Entiendo",
                    value: false,
                    closeModal: false,
                },
                closeOnClickOutside: false,
                closeOnEsc: false,
            });
        },
        alert(p_title, p_text, p_icon) {
            swal({
                title: p_title,
                text: p_text,
                icon: p_icon,
                button: {
                    text: "Ok",
                },
            });
        },
        clearDataModalRecord() {
            this.record.people_id = 0;
            this.record.name = ""
            this.record.lastname = "";
            this.record.quantity = 1;
            this.record.correlative = "";
            this.record.disable.ci = false;
            this.record.disable.names = true;
            this.record.documents = [""];
        },

    },
    computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }

            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }

            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }

            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        },
    },
})
