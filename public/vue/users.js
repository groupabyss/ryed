const users = new Vue({
    el: "#users",
    created() {
        this.loadPage();
    },
    data: {
        users: [],
        route: APP_URL,
        pagination: {
            'total': 0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0
        },
        offset: 1,
        search: "",
        sort: 10,
        user: {
            ci: null,
            name: null,
            lastname: null,
            email: null,
            role: "operator",
            password: null,
        },
        edit: false,
    },
    methods: {
        loadPage() {
            swal({
                title: "Cargando",
                text: "espere un momento mientras se cargan los registros",
                icon: this.route + "/img/loader.gif",
                button: {
                    text: "Ok",
                    value: false,
                    closeModal: false,
                },
                closeOnClickOutside: false,
                closeOnEsc: false,
            });
            this.getUsers();
        },
        getUsers(page) {
            let routed_users = this.route + "/paginate-users?page=" + page + "&sort=" + this.sort + "&name=" + this.search;;
            axios.get(routed_users).then(response => {
                this.users = response.data.users.data;
                this.pagination = response.data.pagination;
                swal.close();
            });
        },
        changePage: function (page) {
            this.pagination.current_page = page;
            this.getUsers(page);
        },
        changeAction(action, user = null) {
            if (action == 'created') {
                this.user = {
                    ci: null,
                    name: null,
                    lastname: null,
                    email: null,
                    role: "operator",
                    password: null,
                }
                this.edit = false;
            } else {
                this.user = user;
                this.edit = true;
            }
            $("#modal-users").modal("show");
        },
        saveUser() {
            phanel.loading("Espere", "Validando y guardando los datos", this.route + "/img/loader.gif");
            axios.post(this.route + "/users", this.user).then(response => {
                phanel.alert("Exito", "Registro creado con exito", "success");
                this.getUsers();
                $("#modal-users").modal("hide");
            }).catch(errors => {
                if (errors.response.status == 422) {
                    Object.values(errors.response.data.errors).forEach(element => {
                        toastr.error(element);
                        swal.close();

                    });;
                } else {
                    console.log(errors.response);
                }
            });
        },
        updateUser() {
            phanel.loading("Espere", "Validando y guardando los datos", this.route + "/img/loader.gif");
            axios.put(this.route + "/users", this.user).then(response => {
                phanel.alert("Exito", "Registro creado con exito", "success");
                this.getUsers();
                $("#modal-users").modal("hide");
            }).catch(errors => {
                if (errors.response.status == 422) {
                    Object.values(errors.response.data.errors).forEach(element => {
                        toastr.error(element);
                        swal.close();
                    });;
                } else {
                    console.log(errors.response);
                }
            });
        },
        deleteUser(id) {
            swal({
                title: "¿Esta Seguro?",
                text: "desea eliminar este usuario y perder todos sus datos",
                icon: "warning",
                buttons: ['no', 'si,estoy seguro'],
                dangerMode: true,
            }).then((acept) => {
                if (acept) {
                    phanel.loading("Espere", "Eliminando Usuario", this.route + "/img/loader.gif");
                    axios.delete(this.route + "/users/" + id).then(response => {
                        this.getUsers();
                        phanel.alert("Exito", "Usuario eliminado con exito", "success");
                    })
                }
            });
        }
    },
    computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }

            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }

            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }

            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        },
    },
})
