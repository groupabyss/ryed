<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'El dato :attribute must be accepted.',
    'active_url' => 'El dato :attribute is not a valid URL.',
    'after' => 'El dato :attribute must be a date after :date.',
    'after_or_equal' => 'El dato :attribute must be a date after or equal to :date.',
    'alpha' => 'El dato :attribute may only contain letters.',
    'alpha_dash' => 'El dato :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'El dato :attribute may only contain letters and numbers.',
    'array' => 'El dato :attribute must be an array.',
    'before' => 'El dato :attribute must be a date before :date.',
    'before_or_equal' => 'El dato :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'El dato :attribute must be between :min and :max.',
        'file' => 'El dato :attribute must be between :min and :max kilobytes.',
        'string' => 'El dato :attribute must be between :min and :max characters.',
        'array' => 'El dato :attribute must have between :min and :max items.',
    ],
    'boolean' => 'El dato :attribute field must be true or false.',
    'confirmed' => 'El dato :attribute confirmation does not match.',
    'date' => 'El dato :attribute is not a valid date.',
    'date_equals' => 'El dato :attribute must be a date equal to :date.',
    'date_format' => 'El dato :attribute does not match the format :format.',
    'different' => 'El dato :attribute and :other must be different.',
    'digits' => 'El dato :attribute must be :digits digits.',
    'digits_between' => 'El dato :attribute must be between :min and :max digits.',
    'dimensions' => 'El dato :attribute has invalid image dimensions.',
    'distinct' => 'El dato :attribute field has a duplicate value.',
    'email' => 'El dato :attribute must be a valid email address.',
    'exists' => 'El dato selected :attribute is invalid.',
    'file' => 'El dato :attribute must be a file.',
    'filled' => 'El dato :attribute field must have a value.',
    'gt' => [
        'numeric' => 'El dato :attribute must be greater than :value.',
        'file' => 'El dato :attribute must be greater than :value kilobytes.',
        'string' => 'El dato :attribute must be greater than :value characters.',
        'array' => 'El dato :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'El dato :attribute must be greater than or equal :value.',
        'file' => 'El dato :attribute must be greater than or equal :value kilobytes.',
        'string' => 'El dato :attribute must be greater than or equal :value characters.',
        'array' => 'El dato :attribute must have :value items or more.',
    ],
    'image' => 'El dato :attribute must be an image.',
    'in' => 'El dato selected :attribute is invalid.',
    'in_array' => 'El dato :attribute field does not exist in :other.',
    'integer' => 'El dato :attribute must be an integer.',
    'ip' => 'El dato :attribute must be a valid IP address.',
    'ipv4' => 'El dato :attribute must be a valid IPv4 address.',
    'ipv6' => 'El dato :attribute must be a valid IPv6 address.',
    'json' => 'El dato :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'El dato :attribute must be less than :value.',
        'file' => 'El dato :attribute must be less than :value kilobytes.',
        'string' => 'El dato :attribute must be less than :value characters.',
        'array' => 'El dato :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'El dato :attribute must be less than or equal :value.',
        'file' => 'El dato :attribute must be less than or equal :value kilobytes.',
        'string' => 'El dato :attribute must be less than or equal :value characters.',
        'array' => 'El dato :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'El dato :attribute may not be greater than :max.',
        'file' => 'El dato :attribute may not be greater than :max kilobytes.',
        'string' => 'El dato :attribute may not be greater than :max characters.',
        'array' => 'El dato :attribute may not have more than :max items.',
    ],
    'mimes' => 'El dato :attribute must be a file of type: :values.',
    'mimetypes' => 'El dato :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'Debe registar al menos :min :attribute.',
        'file' => 'El dato :attribute must be at least :min kilobytes.',
        'string' => 'El dato :attribute must be at least :min characters.',
        'array' => 'El dato :attribute must have at least :min items.',
    ],
    'not_in' => 'El dato selected :attribute is invalid.',
    'not_regex' => 'El dato :attribute format is invalid.',
    'numeric' => 'El dato :attribute must be a number.',
    'present' => 'El dato :attribute field must be present.',
    'regex' => 'El dato :attribute format is invalid.',
    'required' => 'El dato :attribute es requerido.',
    'required_if' => 'El dato :attribute field is required when :other is :value.',
    'required_unless' => 'El dato :attribute field is required unless :other is in :values.',
    'required_with' => 'El dato :attribute field is required when :values is present.',
    'required_with_all' => 'El dato :attribute field is required when :values are present.',
    'required_without' => 'El dato :attribute field is required when :values is not present.',
    'required_without_all' => 'El dato :attribute field is required when none of :values are present.',
    'same' => 'El dato :attribute and :other must match.',
    'size' => [
        'numeric' => 'El dato :attribute must be :size.',
        'file' => 'El dato :attribute must be :size kilobytes.',
        'string' => 'El dato :attribute must be :size characters.',
        'array' => 'El dato :attribute must contain :size items.',
    ],
    'string' => 'El dato :attribute must be a string.',
    'timezone' => 'El dato :attribute must be a valid zone.',
    'unique' => 'El dato :attribute esta repetido.',
    'uploaded' => 'El dato :attribute failed to upload.',
    'url' => 'El dato :attribute format is invalid.',
    'uuid' => 'El dato :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
