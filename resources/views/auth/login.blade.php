@extends('layouts.app') 
@section('title','Login') 
@section('content')
<div class=" container">
    <div class="row full-height  align-items-center ">
        <div class="box-login display-login full-width col-md-6 col-lg-5 offset-md-6 offset-lg-7 box-transparent">
            <form method="POST" action="{{route('login')}}" class=" p-2 rounded text-dark full-width">
                @csrf
                <div class="text-center">
                    <h1 class="display-1 title-login text-primary">RYED</h1>
                </div>
                <div class="form-group ">
                    <label><i class="fas fa-envelope "></i> Correo Electronico</label>
                    <input type="email " class="form-control border border-primary " required name="email" value="{{ old( 'email') }}" required
                        autofocus></div>
                <div class="form-group ">
                    <label><i class="fas fa-key "></i> Contraseña</label>
                    <input type="password" class="form-control border border-primary " required="required" name="password" required>
                </div>
                @if ($errors->any())
                <div class="my-2 text-center ">
                    <strong class="text-center pt-2 ">Lo siento , Credenciales Incorrectas.</strong>
                </div>
                @endif
                <button class="btn btn-primary btn-block ">Ingresar</button>
            </form>
        </div>
    </div>
</div>
@endsection