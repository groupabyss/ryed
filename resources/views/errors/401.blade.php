@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h1 class="title-login display-4 text-white">Disculpe , no tiene permisos para acceder a esta zona.</h1>
        </div>
        <div class="col-sm-6 offset-sm-3 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-center ">
            <div class="bg-white background-img rounded-circle" style="width: 100%;height: 300px; background-image: url('{{ asset('/img/403.png') }}');">

            </div>
        </div>
        <div class="col-12 text-center mt-3">
            <a href="{{ asset('/panel-principal') }}" class="btn btn-primary"><i class="fa fa-home"></i> Regresar a la pagina principal</a>
        </div>
    </div>
</div>
@endsection