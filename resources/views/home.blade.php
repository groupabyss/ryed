@extends('layouts.app') 
@section('content')
    @include("layouts.nav")
<div class="container pb-2" id="phanel">
    <div class="row mt-5 box-transparent p-2 rounded">
        <div class="col-12">
            <h3><i class="fas fa-table"></i> Registro de Documentos <button @click="changeAction('receive')" class="btn btn-xs btn-primary float-right"
                    data-toggle="modal" data-target="#modal-record"><i class="fas fa-plus-circle"></i>
                    Nuevos
                    Documentos</button></h3>
            <div class="row">
                <div class="col-md-2 my-1">
                    <select v-model="sort" class="form-control" @change="getRecords()">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="col-md-2 offset-md-8 my-1">
                    <input type="text" class="form-control" v-model="search" @keyup="getRecords()" placeholder="Buscar Cedula">
                </div>
            </div>
            <div class="table-responsive">
                <table class="shadow table table-bordered table-hover table-striped table-primary">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th>N° Correlatino</th>
                            <th><i class="fas fa-list-alt"></i> Cedula</th>
                            <th><i class="fas fa-user"></i> Dueño</th>
                            <th>Cant Documentos</i>
                            </th>
                            <th><i class="fas fa-calendar-plus"></i> Recibido el</th>
                            <th><i class="fas fa-calendar-minus"></i> Entregado el</th>
                            <th><i class="fas fa-cogs"></i> Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="record in records">
                            <td>@{{record.correlative}}</td>
                            <td>@{{record.peoples[0].ci}}</td>
                            <td>@{{record.peoples[0].name +" "+ record.peoples[0].lastname}}</td>
                            <td>@{{record.documents.length}}</td>
                            <td>@{{record.created_at.split(" ")[0]}}</td>
                            <td :class="{ 'text-center': record.delivery_date == null }">
                                <span class="badge badge-warning" v-if="record.delivery_date == null">Sin Entregar</span>                                @{{record.delivery_date}}
                            </td>
                            <td>
                                <button @click="setDocumentList(record.documents)" class="btn btn-info btn-sm" title="Lista de documentos">
                                        Ver Doc
                                    </button>
                                <button v-if="record.delivery_date == null" @click="changeAction('delivery',record)" class="btn btn-success btn-sm" title="Entregar">
                                        Entregar
                                    </button>
                                <button @click="show_info(record)" v-else class="btn btn-primary btn-sm" title="Ver Datos del registro">
                                        Ver Datos
                                    </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <ul class="pagination">
                <li class="page-item" v-if="pagination.current_page > 1">
                    <span class="page-link" @click.prevent="changePage(pagination.current_page - 1)"><i class="fas fa-arrow-circle-left"></i>
                            Anterior</span>
                </li>
                <li class="page-item" v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']" v-bind:class="[ page == isActived ? 'active' : '']">
                    <a class="page-link" href="#" @click.prevent="changePage(page)">
                            @{{ page }}
                        </a>
                </li>
                <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                    <a class="page-link" href="#" @click.prevent="changePage(pagination.current_page + 1)">
                            Siguiente <i class="fa fa-arrow-circle-right"></i></a>
                </li>
            </ul>
        </div>
    </div>
    @include('modals.record')
    @include('modals.details')
    @include('modals.reports')
</div>
@endsection
 @push('scripts')
<script src="{{ asset('/vue/phanel.js') }}"></script>








@endpush