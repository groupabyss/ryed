<nav class="navbar navbar-expand-lg navbar-dark bg-primary text-light">
    <a href="{{ url('/panel-principal') }}" class="navbar-brand btn btn-primary"><i class="fas fa-home"></i> Inicio</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            @if (Auth::User()->role == "admin")
            <li class="nav-item">
                <a href="{{ url('/users') }}" class="nav-link btn btn-primary"><i class="fa fa-users"></i> Usuarios</a>
            </li>
            @endif
            <li class="nav-item">
                <a data-toggle="modal" data-target="#modal-reports" class="nav-link btn btn-primary"><i class="fa fa-file-pdf"></i>
                    Imprimir PDF</a>
            </li>
        </ul>

        <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out-alt"></i> Cerrar Sesion
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</nav>