<div id="modal-details" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Datos de Documentos
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    <h5>Datos de Recepcion</h5>
                </div>
                <div class="col-md-3">
                    <label>Dueño</label>
                    <input disabled class="form-control" :value="info_record.peoples[0].name + ' ' + info_record.peoples[0].lastname">
                </div>
                <div class="col-md-3">
                    <label>Cedula</label>
                    <input disabled class="form-control" :value="info_record.peoples[0].ci">
                </div>
                <div class="col-md-3">
                    <label>Cant Documentos</label>
                    <input disabled class="form-control" :value="info_record.documents.length">
                </div>
                <div class="col-md-3">
                    <label>N° de Correlativo</label>
                    <input disabled class="form-control" :value="info_record.correlative">
                </div>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    <h5>Datos de Entrega</h5>
                </div>
                <template v-if="info_record.peoples.length > 1">
                    <div class="col-md-3">
                        <label>Persona que Retiro</label>
                        <input disabled class="form-control" :value="info_record.peoples[1].name +' '+ info_record.peoples[1].lastname">
                    </div>
                    <div class="col-md-3">
                        <label>Cedula</label>
                        <input disabled class="form-control" :value="info_record.peoples[1].ci">
                    </div>
                    <div class="col-md-3">
                        <label>Autorizacion</label>
                        <input disabled class="form-control" :value="info_record.authorization">
                    </div>
                    <div class="col-md-3">
                        <label>Obervacion</label>
                        <input disabled class="form-control" :value="info_record.observation">
                    </div>
                </template>
                <template v-else>
                    <div class="col-md-9">
                        <label>&nbsp;</label>
                        <input disabled class="form-control" value="Los documentos los retiro el dueño">
                    </div>
                    <div class="col-md-3">
                        <label>Obervacion</label>
                        <input disabled class="form-control" :value="info_record.observation">
                    </div>
                </template>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary float-right" data-dismiss="modal">Listo</button>
            </div>
        </div>
    </div>
</div>
