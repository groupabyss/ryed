<div id="modal-record" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <template v-if="record.action == 'receive'">
                        Recepcion
                    </template>
                    <template v-else>
                        Entrega
                    </template>
                    de documentos
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body row">
                {{-- cedula --}}
                <form @submit.prevent="searchPeople" class="col-md-12">
                    <div class="form-group">
                        <label>Cedula</label>
                        <div class="input-group-prepend">
                            <input :disabled="record.disable.ci" type="number" class="form-control" v-model="record.ci">
                            <button :disabled="record.disable.ci" class="btn btn-small btn-info"><i class="fas fa-search "></i></button>
                        </div>
                    </div>
                </form>
                {{-- nombres --}}
                <div class="form-group col-md-6">
                    <label>Nombre</label>
                    <input :disabled="record.disable.names" class="form-control" type="text" v-model="record.name">
                </div>
                {{-- apellidos --}}
                <div class="form-group col-md-6">
                    <label>Apellido</label>
                    <input :disabled="record.disable.names" class="form-control" type="text" v-model="record.lastname">
                </div>
                {{-- correlativo --}}
                <div class="form-group col-md-6">
                    <label>Correlativo</label>
                    <input :disabled="record.disable.record" type="number" class="form-control" v-model="record.correlative">
                </div>
                {{-- documentos --}}
                <div class="form-group col-md-6">
                    <label>Cant Documentos</label>
                    <input :disabled="record.disable.record" type="number" class="form-control" min="1" v-model="record.quantity"
                        @change="charge_documents()">
                </div>
                <div v-for="(document,index) in record.documents" class="col-md-6">
                    <label>Documento N° @{{index+1}}</label>
                    <input :disabled="record.disable.record" type="text" class="form-control" v-model="record.documents[index]"
                        placeholder="nombre documento">
                </div>
                {{-- autorization --}}
                <div class="form-group col-md-12 mt-1" v-show="!record.disable.authorization">
                    <label>Autorizacion </label>
                    <select v-model="record.authorization" class="form-control">
                        <option value="Partida de nacimiento">Partida de nacimiento</option>
                        <option value="Poder notariado">Poder Notariado</option>
                    </select>
                </div>
                {{-- observacion --}}
                <div class="form-group col-md-12 mt-1" v-show="!record.disable.observation">
                    <label>Observacion (opcional)</label>
                    <textarea class="form-control" rows="2" v-model="record.observation"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button @click="saveRecord()" type="submit" v-if="record.action == 'receive'" class="float-right btn btn-small btn-primary"><i
                        class="fas fa-arrow-circle-right"></i> Guardar</button>
                <button @click="saveDelivery()" type="submit" v-else class="float-right btn btn-small btn-primary"><i
                        class="fas fa-arrow-circle-right"></i> Entregar</button>
            </div>
        </div>
    </div>
</div>
