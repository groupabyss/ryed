<div id="modal-reports" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Repotes PDF
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <form id="form-print" method="post" action="{{ route('records.print') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <label>Desde</label>
                                <input @change="validate_date()" v-model="range_f" name="first" class="form-control" type="date">
                            </div>
                            <div class="col-md-6">
                                <label>Hasta</label>
                                <input @change="validate_date()" v-model="range_l" name="last" class="form-control" type="date">
                            </div>
                        </div>
                    </form>
                </ul>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary float-right" @click="print()">Imprimir</button>
            </div>
        </div>
    </div>
</div>