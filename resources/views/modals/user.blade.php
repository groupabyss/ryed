<div id="modal-users" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Usuario
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <div class="col-md-6">
                    <label>Nombre</label>
                    <input class="form-control" v-model="user.name">
                </div>
                <div class="col-md-6">
                    <label>Apellido</label>
                    <input class="form-control" v-model="user.lastname">
                </div>
                <div class="col-md-6">
                    <label>Cedula</label>
                    <input :disabled="edit" class="form-control" v-model="user.ci" type="number">
                </div>
                <div class="col-md-6">
                    <label>Rol</label>
                    <select v-model="user.role" class="form-control">
                        <option value="admin">Administrador</option>
                        <option value="operator">Operador</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Correo</label>
                    <input :disabled="edit" class="form-control" v-model="user.email">
                </div>
                <div class="col-md-6">
                    <label><span v-if="edit">Cambiar</span> Contraseña</label>
                    <input class="form-control" v-model="user.password">
                </div>
            </div>
            <div class="modal-footer">
                <button v-if="!edit" class="btn btn-primary float-right" @click="saveUser()">Registrar</button>
                <button v-else class="btn btn-primary float-right" @click="updateUser()">Actualizar</button>
            </div>
        </div>
    </div>
</div>
