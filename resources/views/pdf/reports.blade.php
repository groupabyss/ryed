<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Reportes</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <style>
        .table {
            font-size: 10px;
            clear: both;
            margin-top: 10px;
        }

        .img-logo {
            width: 10%;
            height: auto;
            float: left;
            display: inline;
        }

        .title-dates {
            width: 90%;
            display: inline-block;
        }
    </style>
</head>

<body>
    <img src="{{ asset('/img/logo.jpg') }}" class="img-logo"> @isset($first)
    <h3 class="title-dates text-center mt-3">Reporte del {{ $first }} al {{$last}}</h3>
    @else
    <h3 class="title-dates text-center mt-3">Reporte de Este {{$type}}</h3>
    @endisset

    <table class="table">
        <thead class="bg-primary text-light">
            <tr>
                <th>Correlativo</th>
                <th>Nombre</th>
                <th>Cedula</th>
                <th>Cant Doc</th>
                <th>Fecha Ingreso</th>
                <th>Fecha Entrega</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($records as $record)
            <tr>
                <td>{{$record->correlative}}</td>
                <td>{{$record->peoples[0]->name ." ".$record->peoples[0]->lastname}}</td>
                <td>{{$record->peoples[0]->ci }}</td>
                <td>{{ $record->documents->count() }}</td>
                <td>{{ date("Y-m-d",strtotime($record->created_at)) }}</td>
                <td>{{ $record->delivery_date }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>