@extends('layouts.app') 
@section('content')
    @include("layouts.nav")
<div class="container" id="users">
    <div class="row mt-2 box-transparent rounded mt-5 p-2">
        <div class="col-12">
            <h3>
                <i class="fas fa-users"></i> Lista de Usuarios
                <button class="btn btn-xs btn-primary float-right" @click="changeAction('created')">
                    <i class="fas fa-plus-circle"></i> Nuevo usuario
                </button>
            </h3>
            <div class="row">
                <div class="col-md-2 my-1">
                    <select v-model="sort" class="form-control" @change="getUsers()">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="col-md-2 offset-md-8 my-1">
                    <input type="text" class="form-control" v-model="search" @keyup="getUsers()" placeholder="Buscar Cedula">
                </div>
            </div>
            <div class="table-responsive">
                <table class="shadow table table-bordered table-hover table-striped table-primary">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Correo</th>
                            <th>Rol</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="user in users">
                            <td v-text="user.name +' '+user.lastname"></td>
                            <td v-text="user.ci"></td>
                            <td v-text="user.email"></td>
                            <td v-text="user.role"></td>
                            <td>
                                <button @click="changeAction('edit',user)" class="btn btn-sm btn-warning">Editar</button>
                                <button @click="deleteUser(user.id)" class="btn btn-sm btn-danger">Eliminar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <ul class="pagination">
                <li class="page-item" v-if="pagination.current_page > 1">
                    <span class="page-link" @click.prevent="changePage(pagination.current_page - 1)"><i class="fas fa-arrow-circle-left"></i>
                            Anterior</span>
                </li>
                <li class="page-item" v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']" v-bind:class="[ page == isActived ? 'active' : '']">
                    <a class="page-link" href="#" @click.prevent="changePage(page)">
                            @{{ page }}
                        </a>
                </li>
                <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                    <a class="page-link" href="#" @click.prevent="changePage(pagination.current_page + 1)">
                            Siguiente <i class="fa fa-arrow-circle-right"></i></a>
                </li>
            </ul>

        </div>
    </div>
    @include('modals.user')
</div>
@endsection
 @push('scripts')


<script src="{{ asset('/vue/phanel.js') }}"></script>
<script src="{{ asset('/vue/users.js') }}"></script>





@endpush