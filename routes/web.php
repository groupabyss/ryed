<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::middleware(['auth'])->group(function() {
    Route::get('/panel-principal', 'HomeController@index');

    //records routes
    Route::get("/records","RecordsController@getRecords")->name("records");
    Route::post("/records","RecordsController@store")->name("records.store");
    Route::put("/records","RecordsController@updated")->name("records.updated");
    Route::post("/records/pdf","RecordsController@print")->name("records.print");
    //peoples
    Route::get("/peoples/{people}","peoplesController@searchPeoples")->name("peoples.search");

    Route::middleware(['admin'])->group(function() {
        Route::get("/users","UsersControllers@index")->middleware('admin')->name("users");
        Route::post("/users","UsersControllers@store")->name("users.store");
        Route::put("/users","UsersControllers@update")->name("users.update");
        Route::delete("/users/{id}","UsersControllers@destroy")->name("users.destroy");
        Route::get("/paginate-users","UsersControllers@getUsers")->name("users.paginate");
    });
    
});